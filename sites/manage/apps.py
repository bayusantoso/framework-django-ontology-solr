# baseframework.apps.py

from django.apps import AppConfig

class BaseframeworkConfig(AppConfig):
    name = 'baseframework'
