# search/views.py
import os
import os.path
import requests

from django.shortcuts import render
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse
from django.middleware import csrf

import rdflib
from haystack import indexes
#import rpy2.robjects as robjects

from search.models import UserType
from search.models import GOntology

#from search.models import FunctionInfo

# Create your views here.

def index(request):
    user_type = UserType()
    user_type.Id = 1
    user_type.Name = "Admin"

    function_info = None

    csrf_token = csrf.get_token(request)
    content = render_to_string("search/index.html", {'model' : function_info, 'csrf_token':csrf_token})
    base_url = 'http://localhost:8000/search/'
    return render(request, "shared/master_public.html", {'content':content,'base_url':base_url})

def searchgo_solr(request):
    function_info = None
    csrf_token = csrf.get_token(request)
    action_post_url = "resultgo_solr"
    content = render_to_string("search/index.html", {'model' : function_info, 'csrf_token':csrf_token, 'action_post_url' : action_post_url})
    base_url = 'http://localhost:8000/search/'
    return render(request, "shared/master_public.html", {'content':content,'base_url':base_url})

def resultgo_solr(request):
    query = request.POST.get("query", "")
    query_result = None
    if query != None and query != "":
        url = "http://localhost:8983/solr/geneontology/select?q=" + query + "&start=0&rows=100000&wt=json"
        query_result = requests.get(url)

    result = None
    if query_result != None:
        result = bind_json_to_list_of_object(query_result)

    csrf_token = csrf.get_token(request)
    base_url = 'http://localhost:8000/search/'
    content = render_to_string("search/result.html",{'csrf_token':csrf_token, 'model' : result, 'query':query,'base_url':base_url})
    return render(request, "shared/master_public.html", {'content':content,'base_url':base_url})

def popup_resultgo_solr(request):
    query = request.GET.get("query", "")
    query_result = None
    if query != None and query != "":
        url = "http://localhost:8983/solr/geneontology/select?q=" + query + "&start=0&rows=100000&wt=json"
        query_result = requests.get(url)

    result = None
    if query_result != None:
        result = bind_json_to_list_of_object(query_result)

    csrf_token = csrf.get_token(request)
    base_url = 'http://localhost:8000/search/'
    content = render_to_string("search/popupresult.html",{'csrf_token':csrf_token, 'model' : result, 'query':query,'base_url':base_url})
    return render(request, "shared/popup_master_public.html", {'content':content,'base_url':base_url})

def viewitem(request):
    base_url = 'http://localhost:8000/search/'
    content = viewitembase(request)
    return render(request, "shared/master_public.html", {'content':content,'base_url':base_url})

def viewitembase(request, id):
    result = None
    query = None
    csrf_token = csrf.get_token(request)
    base_url = 'http://localhost:8000/search/'

    content = render_to_string("search/viewitem.html",{'csrf_token':csrf_token, 'model' : result, 'query':query,'base_url':base_url})
    if id == "1":
        content = render_to_string("search/viewitem.html",{'csrf_token':csrf_token, 'model' : result, 'query':query,'base_url':base_url})
    elif id == "2":
        content = render_to_string("search/viewitem2.html",{'csrf_token':csrf_token, 'model' : result, 'query':query,'base_url':base_url})
    elif id == "3":
        content = render_to_string("search/viewitem3.html",{'csrf_token':csrf_token, 'model' : result, 'query':query,'base_url':base_url})

    return content

def viewitemblank(request):
    base_url = 'http://localhost:8000/search/'
    id = request.GET.get("id", "")
    content = viewitembase(request, id)
    return render(request, "shared/blank.html", {'content':content,'base_url':base_url})

def menu(request):
    id = request.GET.get("id", "")
    csrf_token = csrf.get_token(request)
    if id == "1":
        content = render_to_string("menu/menu1.html",{'csrf_token':csrf_token})
    else:
        content = render_to_string("menu/menu.html",{'csrf_token':csrf_token})

    base_url = 'http://localhost:8000/search/'
    return render(request, "shared/blank.html", {'content':content,'base_url':base_url})

str_parent = []
span_parent = {}
def detailgo(request):
    query = request.GET.get("query", "")
    id = request.GET.get("id", "")
    go_id = id
    query_result = None

    if id != None and id != "":
        url = "http://localhost:8983/solr/geneontology/select?q=id:\"" + id + "\"&wt=json"
        query_result = requests.get(url)

    result = None
    if query_result != None:
        result = bind_json_to_object(query_result, go_id)

    csrf_token = csrf.get_token(request)
    base_url = 'http://localhost:8000/search/'
    search_url = 'http://localhost:8000/search/resultgo_solr/'

    data = {'csrf_token':csrf_token,
            'model' : result,
            'query':query,
            'base_url':base_url,
            'search_url':search_url,
            'query_result':query_result,
            'dict_hierarcys':str_parent,
            'span_parent':span_parent,
            'tab' : (len(str_parent)+1)*10 }

    content = render_to_string("search/detail.html",data)
    str_parent.clear()
    span_parent.clear()
    return render(request, "shared/master_public.html", {'content':content,'base_url':base_url})

def popup_detail(request):
    query = request.GET.get("query", "")
    id = request.GET.get("id", "")
    go_id = id
    query_result = None

    if id != None and id != "":
        url = "http://localhost:8983/solr/geneontology/select?q=id:\"" + id + "\"&wt=json"
        query_result = requests.get(url)

    result = None
    if query_result != None:
        result = bind_json_to_object(query_result, go_id)

    csrf_token = csrf.get_token(request)
    base_url = 'http://localhost:8000/search/'
    search_url = 'http://localhost:8000/search/resultgo_solr/'

    content = render_to_string("search/popupdetail.html",{'csrf_token':csrf_token, 'model' : result, 'query':query,'base_url':base_url,'search_url':search_url, 'query_result':query_result, 'dict_hierarcys':str_parent,'span_parent':span_parent, 'tab' : (len(str_parent)+1)*10 })
    str_parent.clear()
    span_parent.clear()
    return render(request, "shared/popup_master_public.html", {'content':content,'base_url':base_url})

def get_object_hierarchy(obj):
    if obj == None:
        return ""

    if len(obj.go_hierarcy) <= 0:
        return ""

    str = ""
    item = obj.go_hierarcy[0]
    if item != None:
        if item.go_id == "all":
            str += item.go_id
        else:
            str += item.go_id + " | "

    return str


def bind_json_to_object(response, id):
    go_onto = None
    result = response.json()
    if result == None:
        return go_onto

    result_response = None
    if "response" in result:
        result_response = result["response"]

    if result_response == None:
        return go_onto

    if len(result_response["docs"]) > 0:
        doc = result_response["docs"][0]
        gene_ontology = GOntology()
        if "id" in doc:
            gene_ontology.go_id = doc["id"]
        if "go_name" in doc:
            gene_ontology.go_name = doc["go_name"]
        if "go_definition" in doc:
            gene_ontology.go_definition = doc["go_definition"]

        if "is_a" in doc:
            gene_ontology.go_is_a = []
            num = 1
            for is_a in doc["is_a"]:
                if num > 1:
                    break

                gene_ontology.go_is_a.append(is_a[31:])
                object_of = get_solr_hierarchy(is_a[31:])
                if object_of != None:
                    str_parent.append(object_of)
                    if len(str_parent) > 0:
                        str_span = 0

                        for item in str_parent:
                            str_span += 10

                        span_parent[object_of.go_id] = str_span
                    num += 1


        if "go_synonym" in doc:
            gene_ontology.go_synonym = doc["go_synonym"]

        go_onto = gene_ontology

    return go_onto

def bind_json_to_list_of_object(response):
    go_onto = []
    result = response.json()

    if result == None:
        return go_onto

    result_response = None
    if "response" in result:
        result_response = result["response"]

    if result_response == None:
        return go_onto

    if len(result_response["docs"]) > 0:
        num = 1
        for doc in result_response["docs"]:
            gene_ontology = GOntology()

            go_id = doc["id"]
            gene_ontology.go_num = num
            gene_ontology.go_id = go_id
            gene_ontology.go_name = doc["go_name"]
            gene_ontology.go_definition = doc["go_definition"]

            if "is_a" in doc:
                gene_ontology.go_is_a = doc["is_a"]

            if "go_synonym" in doc:
                gene_ontology.go_synonym = doc["go_synonym"]

            num += 1
            go_onto.append(gene_ontology)

    return go_onto

def get_solr_hierarchy(id):
    go_onto = None

    if id == "all":
        return None

    if id != None and id != "":
        url = "http://localhost:8983/solr/geneontology/select?q=id:\"" + id + "\"&wt=json"
        query_result = requests.get(url)

        if query_result != None:
            go_onto = bind_json_to_object(query_result, id)

    return go_onto
