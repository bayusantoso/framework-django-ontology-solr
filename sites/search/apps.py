# baseframework.apps.py

from django.apps import AppConfig

class SearchConfig(AppConfig):
    name = 'search'
