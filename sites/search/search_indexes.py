from haystack import indexes
from search.models import GOntology

class GOntologyIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    id = indexes.CharField(model_attr='go_id', faceted=True)
    go_name = indexes.CharField(model_attr='go_name', faceted=True)
    go_definition = indexes.CharField(model_attr='go_definition', faceted=True)

    def get_model(self):
        return GOntology

    def index_queryset(self):
        return self.get_model().objects.all()