-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.6.20 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for base_framework_django
DROP DATABASE IF EXISTS `base_framework_django`;
CREATE DATABASE IF NOT EXISTS `base_framework_django` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `base_framework_django`;


-- Dumping structure for table base_framework_django.auth_group
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.auth_group: ~0 rows (approximately)
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.auth_group_permissions
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.auth_group_permissions: ~0 rows (approximately)
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.auth_permission
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.auth_permission: ~48 rows (approximately)
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
	(1, 'Can add log entry', 1, 'add_logentry'),
	(2, 'Can change log entry', 1, 'change_logentry'),
	(3, 'Can delete log entry', 1, 'delete_logentry'),
	(4, 'Can add permission', 2, 'add_permission'),
	(5, 'Can change permission', 2, 'change_permission'),
	(6, 'Can delete permission', 2, 'delete_permission'),
	(7, 'Can add group', 3, 'add_group'),
	(8, 'Can change group', 3, 'change_group'),
	(9, 'Can delete group', 3, 'delete_group'),
	(10, 'Can add user', 4, 'add_user'),
	(11, 'Can change user', 4, 'change_user'),
	(12, 'Can delete user', 4, 'delete_user'),
	(13, 'Can add content type', 5, 'add_contenttype'),
	(14, 'Can change content type', 5, 'change_contenttype'),
	(15, 'Can delete content type', 5, 'delete_contenttype'),
	(16, 'Can add session', 6, 'add_session'),
	(17, 'Can change session', 6, 'change_session'),
	(18, 'Can delete session', 6, 'delete_session'),
	(19, 'Can add user type', 7, 'add_usertype'),
	(20, 'Can change user type', 7, 'change_usertype'),
	(21, 'Can delete user type', 7, 'delete_usertype'),
	(22, 'Can add functions', 8, 'add_functions'),
	(23, 'Can change functions', 8, 'change_functions'),
	(24, 'Can delete functions', 8, 'delete_functions'),
	(25, 'Can add image taging', 9, 'add_imagetaging'),
	(26, 'Can change image taging', 9, 'change_imagetaging'),
	(27, 'Can delete image taging', 9, 'delete_imagetaging'),
	(28, 'Can add modules', 10, 'add_modules'),
	(29, 'Can change modules', 10, 'change_modules'),
	(30, 'Can delete modules', 10, 'delete_modules'),
	(31, 'Can add user groups', 11, 'add_usergroups'),
	(32, 'Can change user groups', 11, 'change_usergroups'),
	(33, 'Can delete user groups', 11, 'delete_usergroups'),
	(34, 'Can add golden list', 12, 'add_goldenlist'),
	(35, 'Can change golden list', 12, 'change_goldenlist'),
	(36, 'Can delete golden list', 12, 'delete_goldenlist'),
	(37, 'Can add golden list model', 13, 'add_goldenlistmodel'),
	(38, 'Can change golden list model', 13, 'change_goldenlistmodel'),
	(39, 'Can delete golden list model', 13, 'delete_goldenlistmodel'),
	(40, 'Can add users', 14, 'add_users'),
	(41, 'Can change users', 14, 'change_users'),
	(42, 'Can delete users', 14, 'delete_users'),
	(43, 'Can add privileges', 15, 'add_privileges'),
	(44, 'Can change privileges', 15, 'change_privileges'),
	(45, 'Can delete privileges', 15, 'delete_privileges'),
	(46, 'Can add evaluation', 16, 'add_evaluation'),
	(47, 'Can change evaluation', 16, 'change_evaluation'),
	(48, 'Can delete evaluation', 16, 'delete_evaluation');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.auth_user
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) COLLATE utf8_bin NOT NULL,
  `first_name` varchar(30) COLLATE utf8_bin NOT NULL,
  `last_name` varchar(30) COLLATE utf8_bin NOT NULL,
  `email` varchar(254) COLLATE utf8_bin NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.auth_user: ~0 rows (approximately)
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.auth_user_groups
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.auth_user_groups: ~0 rows (approximately)
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.auth_user_user_permissions
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.auth_user_user_permissions: ~0 rows (approximately)
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.baseframework_usertype
DROP TABLE IF EXISTS `baseframework_usertype`;
CREATE TABLE IF NOT EXISTS `baseframework_usertype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.baseframework_usertype: ~0 rows (approximately)
/*!40000 ALTER TABLE `baseframework_usertype` DISABLE KEYS */;
/*!40000 ALTER TABLE `baseframework_usertype` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.django_admin_log
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext COLLATE utf8_bin,
  `object_repr` varchar(200) COLLATE utf8_bin NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext COLLATE utf8_bin NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.django_admin_log: ~0 rows (approximately)
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.django_content_type
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) COLLATE utf8_bin NOT NULL,
  `model` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.django_content_type: ~14 rows (approximately)
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
	(1, 'admin', 'logentry'),
	(3, 'auth', 'group'),
	(2, 'auth', 'permission'),
	(4, 'auth', 'user'),
	(7, 'baseframework', 'usertype'),
	(5, 'contenttypes', 'contenttype'),
	(16, 'evaluation', 'evaluation'),
	(8, 'functions', 'functions'),
	(12, 'golden_list', 'goldenlist'),
	(13, 'golden_list', 'goldenlistmodel'),
	(9, 'image_tagging', 'imagetaging'),
	(10, 'modules', 'modules'),
	(15, 'privileges', 'privileges'),
	(6, 'sessions', 'session'),
	(11, 'user_groups', 'usergroups'),
	(14, 'users', 'users');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.django_migrations
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE IF NOT EXISTS `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) COLLATE utf8_bin NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.django_migrations: ~36 rows (approximately)
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
	(1, 'contenttypes', '0001_initial', '2016-04-02 15:36:11.579637'),
	(2, 'auth', '0001_initial', '2016-04-02 15:36:22.778278'),
	(3, 'admin', '0001_initial', '2016-04-02 15:36:27.269534'),
	(4, 'admin', '0002_logentry_remove_auto_add', '2016-04-02 15:36:27.481547'),
	(5, 'contenttypes', '0002_remove_content_type_name', '2016-04-02 15:36:29.116640'),
	(6, 'auth', '0002_alter_permission_name_max_length', '2016-04-02 15:36:30.524721'),
	(7, 'auth', '0003_alter_user_email_max_length', '2016-04-02 15:36:32.289822'),
	(8, 'auth', '0004_alter_user_username_opts', '2016-04-02 15:36:32.360826'),
	(9, 'auth', '0005_alter_user_last_login_null', '2016-04-02 15:36:33.030864'),
	(10, 'auth', '0006_require_contenttypes_0002', '2016-04-02 15:36:33.128870'),
	(11, 'auth', '0007_alter_validators_add_error_messages', '2016-04-02 15:36:33.201874'),
	(12, 'baseframework', '0001_initial', '2016-04-02 16:17:35.665719'),
	(13, 'sessions', '0001_initial', '2016-04-02 16:17:36.457764'),
	(14, 'functions', '0001_initial', '2016-09-27 10:19:14.268804'),
	(15, 'functions', '0002_auto_20160927_1723', '2016-09-27 10:24:12.433384'),
	(16, 'functions', '0003_auto_20160929_2138', '2016-09-29 14:39:17.452966'),
	(17, 'functions', '0004_auto_20160929_2139', '2016-09-29 14:39:17.522005'),
	(18, 'functions', '0005_auto_20160929_2146', '2016-09-29 14:46:52.380593'),
	(19, 'functions', '0006_remove_functions_sequence', '2016-09-29 15:06:14.207409'),
	(20, 'functions', '0007_functions_sequence', '2016-09-29 15:08:12.587161'),
	(21, 'image_tagging', '0001_initial', '2016-09-29 21:08:03.568722'),
	(22, 'functions', '0008_functions_icon', '2016-09-30 01:45:16.773373'),
	(23, 'modules', '0001_initial', '2016-10-04 11:24:59.044579'),
	(24, 'user_groups', '0001_initial', '2016-10-04 11:41:45.158385'),
	(25, 'golden_list', '0001_initial', '2016-10-23 10:42:56.781616'),
	(26, 'users', '0001_initial', '2016-10-23 10:54:28.363873'),
	(27, 'users', '0002_auto_20161023_1756', '2016-10-23 10:56:11.314744'),
	(28, 'evaluation', '0001_initial', '2017-02-25 11:14:23.150184'),
	(29, 'privileges', '0001_initial', '2017-02-25 11:14:28.025354'),
	(30, 'users', '0003_users_name', '2017-02-25 11:14:28.994037'),
	(31, 'privileges', '0002_privileges_is_show', '2017-02-25 11:15:12.281060'),
	(32, 'privileges', '0003_remove_privileges_user_group_id', '2017-02-25 12:23:55.561564'),
	(33, 'user_groups', '0002_usergroups_privileges', '2017-02-25 12:23:57.577241'),
	(34, 'user_groups', '0003_remove_usergroups_privileges', '2017-02-25 12:29:58.131139'),
	(35, 'privileges', '0004_privileges_user_group_id', '2017-02-25 12:30:00.584726'),
	(36, 'functions', '0009_functions_is_public', '2017-02-25 13:28:01.135505'),
	(37, 'privileges', '0005_auto_20170225_2315', '2017-02-25 16:15:50.309930'),
	(38, 'users', '0004_auto_20170226_0644', '2017-02-25 23:44:18.357638');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.django_session
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) COLLATE utf8_bin NOT NULL,
  `session_data` longtext COLLATE utf8_bin NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.django_session: ~2 rows (approximately)
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
	('idpltnvoisjyporxa89702os7tlwdk2m', 'YWIwYzZhNTc1OTNjYTZiZjgwMDU3YzA3NWJlOGI4Zjk3OGIyZTg5YTp7ImlzX2xvZ2luIjp0cnVlLCJ1c2VyX25hbWUiOiJhZG1pbiIsInVzZXJpbmZvIjoiYWRtaW4ifQ==', '2017-02-09 07:54:57.452650'),
	('zss0i52u3qgeb6obkmr5y5isme50sy2v', 'MDM5OGRjNmRjMjA4NTllZDk4MTVlNTdjZGQ3N2FiMzhiMTZhODJhMjp7InByaXZpbGVnZXMiOnsiZXZhbHVhdGlvbl9pc19hbGxvd19kZWxldGUiOnRydWUsInVzZXJzX2lzX2FsbG93X3JlYWQiOnRydWUsInVzZXJzX2lzX2FsbG93X2NyZWF0ZSI6dHJ1ZSwiZnVuY3Rpb25zX2lzX2FsbG93X3JlYWQiOnRydWUsImltYWdlX3RhZ2luZ19pc19hbGxvd191cGRhdGUiOnRydWUsInVzZXJfZ3JvdXBzX2lzX2FsbG93X3JlYWQiOnRydWUsIm1vZHVsZXNfaXNfYWxsb3dfdXBkYXRlIjp0cnVlLCJ1c2VyX2dyb3Vwc19pc19hbGxvd191cGRhdGUiOnRydWUsImltYWdlX3RhZ2luZ19pc19hbGxvd19kZWxldGUiOnRydWUsInVzZXJzX2lzX2FsbG93X2RlbGV0ZSI6dHJ1ZSwidXNlcl9ncm91cHNfaXNfYWxsb3dfZGVsZXRlIjp0cnVlLCJtb2R1bGVzX2lzX2FsbG93X2RlbGV0ZSI6dHJ1ZSwibW9kdWxlc19pc19hbGxvd19yZWFkIjp0cnVlLCJ1c2Vyc19pc19hbGxvd191cGRhdGUiOnRydWUsImZ1bmN0aW9uc19pc19hbGxvd191cGRhdGUiOnRydWUsImZ1bmN0aW9uc19pc19hbGxvd19kZWxldGUiOnRydWUsInVzZXJfZ3JvdXBzX2lzX2FsbG93X2NyZWF0ZSI6dHJ1ZSwiZXZhbHVhdGlvbl9pc19hbGxvd19yZWFkIjp0cnVlLCJldmFsdWF0aW9uX2lzX2FsbG93X3VwZGF0ZSI6dHJ1ZSwiaW1hZ2VfdGFnaW5nX2lzX2FsbG93X2NyZWF0ZSI6dHJ1ZSwiaW1hZ2VfdGFnaW5nX2lzX2FsbG93X3JlYWQiOnRydWUsIm1vZHVsZXNfaXNfYWxsb3dfY3JlYXRlIjp0cnVlLCJmdW5jdGlvbnNfaXNfYWxsb3dfY3JlYXRlIjp0cnVlLCJldmFsdWF0aW9uX2lzX2FsbG93X2NyZWF0ZSI6dHJ1ZX0sInVzZXJfbmFtZSI6ImFkbWluIiwiaXNfbG9naW4iOnRydWUsImZ1bmN0aW9uaWQiOiJldmFsdWF0aW9uIn0=', '2017-03-13 03:24:09.171187');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.evaluation_evaluation
DROP TABLE IF EXISTS `evaluation_evaluation`;
CREATE TABLE IF NOT EXISTS `evaluation_evaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `created_by` varchar(100) COLLATE utf8_bin NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `updated_by` varchar(100) COLLATE utf8_bin NOT NULL,
  `updated_date` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.evaluation_evaluation: ~0 rows (approximately)
/*!40000 ALTER TABLE `evaluation_evaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `evaluation_evaluation` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.functions_functions
DROP TABLE IF EXISTS `functions_functions`;
CREATE TABLE IF NOT EXISTS `functions_functions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `url` varchar(100) COLLATE utf8_bin NOT NULL,
  `function_id` varchar(100) COLLATE utf8_bin NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_show` tinyint(1) NOT NULL,
  `sequence` int(11) NOT NULL,
  `icon` varchar(100) COLLATE utf8_bin NOT NULL,
  `is_public` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.functions_functions: ~7 rows (approximately)
/*!40000 ALTER TABLE `functions_functions` DISABLE KEYS */;
INSERT INTO `functions_functions` (`id`, `name`, `url`, `function_id`, `is_active`, `is_show`, `sequence`, `icon`, `is_public`) VALUES
	(7, 'Leaf', 'media/view/5', 'plant', 1, 1, 0, 'static/images/2016/09/30/leaftree.jpg', 1),
	(8, 'Functions', '/functions/', 'functions', 1, 1, 1, '', 0),
	(9, 'Modules', '/modules/', 'modules', 1, 1, 2, '', 0),
	(10, 'User Groups', '/usergroups/', 'user_groups', 1, 1, 3, '', 0),
	(11, 'Users', '/users/', 'users', 1, 1, 4, '', 0),
	(12, 'Image Taging', '/imagetaging/', 'image_taging', 1, 1, 1, '', 0),
	(13, 'Evaluation', '/evaluation/', 'evaluation', 1, 1, 2, '', 0);
/*!40000 ALTER TABLE `functions_functions` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.golden_list_goldenlist
DROP TABLE IF EXISTS `golden_list_goldenlist`;
CREATE TABLE IF NOT EXISTS `golden_list_goldenlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `created_by` varchar(100) COLLATE utf8_bin NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `updated_by` varchar(100) COLLATE utf8_bin NOT NULL,
  `updated_date` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.golden_list_goldenlist: ~0 rows (approximately)
/*!40000 ALTER TABLE `golden_list_goldenlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `golden_list_goldenlist` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.golden_list_goldenlistmodel
DROP TABLE IF EXISTS `golden_list_goldenlistmodel`;
CREATE TABLE IF NOT EXISTS `golden_list_goldenlistmodel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `onto_id` varchar(100) COLLATE utf8_bin NOT NULL,
  `onto_query` varchar(100) COLLATE utf8_bin NOT NULL,
  `golden_list_id_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `golden_list_id_id` (`golden_list_id_id`),
  CONSTRAINT `golden_l_golden_list_id_id_23a75024_fk_golden_list_goldenlist_id` FOREIGN KEY (`golden_list_id_id`) REFERENCES `golden_list_goldenlist` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.golden_list_goldenlistmodel: ~0 rows (approximately)
/*!40000 ALTER TABLE `golden_list_goldenlistmodel` DISABLE KEYS */;
/*!40000 ALTER TABLE `golden_list_goldenlistmodel` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.image_tagging_imagetaging
DROP TABLE IF EXISTS `image_tagging_imagetaging`;
CREATE TABLE IF NOT EXISTS `image_tagging_imagetaging` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `image_file` varchar(100) COLLATE utf8_bin NOT NULL,
  `taging_data` varchar(1000) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.image_tagging_imagetaging: ~2 rows (approximately)
/*!40000 ALTER TABLE `image_tagging_imagetaging` DISABLE KEYS */;
INSERT INTO `image_tagging_imagetaging` (`id`, `name`, `image_file`, `taging_data`) VALUES
	(5, 'Leaf Anatomy', 'static/images/2016/09/30/leaf.jpg', '[{"x":0.41418650793650796,"y":0.75,"text":"xylem"},{"x":0.5391865079365079,"y":0.7355371900826446,"text":"phloem"},{"x":0.29117063492063494,"y":0.40082644628099173,"text":"palisade meshopyll"},{"x":0.16815476190476192,"y":0.6136363636363636,"text":"meshopyll"},{"x":0.41617063492063494,"y":0.33884297520661155,"text":"upper epidermis"},{"x":0.5173611111111112,"y":0.9173553719008265,"text":"lower epidermis"},{"x":0.4439484126984127,"y":0.27892561983471076,"text":"cuticle"},{"x":0.714781746031746,"y":0.41735537190082644,"text":"meshopyll"},{"x":0.17162698412698413,"y":0.7527548261910431,"text":"sponge"}]');
/*!40000 ALTER TABLE `image_tagging_imagetaging` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.modules_modules
DROP TABLE IF EXISTS `modules_modules`;
CREATE TABLE IF NOT EXISTS `modules_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.modules_modules: ~0 rows (approximately)
/*!40000 ALTER TABLE `modules_modules` DISABLE KEYS */;
INSERT INTO `modules_modules` (`id`, `name`) VALUES
	(1, 'Privilege Management'),
	(2, 'Application');
/*!40000 ALTER TABLE `modules_modules` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.privileges_privileges
DROP TABLE IF EXISTS `privileges_privileges`;
CREATE TABLE IF NOT EXISTS `privileges_privileges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_allow_read` tinyint(1) NOT NULL,
  `is_allow_create` tinyint(1) NOT NULL,
  `is_allow_update` tinyint(1) NOT NULL,
  `is_allow_delete` tinyint(1) NOT NULL,
  `function_id_id` int(11) DEFAULT NULL,
  `module_id_id` int(11) DEFAULT NULL,
  `is_show` tinyint(1) NOT NULL,
  `user_group_id_id` int(11),
  PRIMARY KEY (`id`),
  KEY `privileges_privileges_779e10eb` (`user_group_id_id`),
  KEY `function_id_id` (`function_id_id`),
  KEY `module_id_id` (`module_id_id`),
  CONSTRAINT `privilege_user_group_id_id_462a6f97_fk_user_groups_usergroups_id` FOREIGN KEY (`user_group_id_id`) REFERENCES `user_groups_usergroups` (`id`),
  CONSTRAINT `privileges_pri_function_id_id_7b71a3d7_fk_functions_functions_id` FOREIGN KEY (`function_id_id`) REFERENCES `functions_functions` (`id`),
  CONSTRAINT `privileges_privilege_module_id_id_63b24a71_fk_modules_modules_id` FOREIGN KEY (`module_id_id`) REFERENCES `modules_modules` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.privileges_privileges: ~6 rows (approximately)
/*!40000 ALTER TABLE `privileges_privileges` DISABLE KEYS */;
INSERT INTO `privileges_privileges` (`id`, `is_allow_read`, `is_allow_create`, `is_allow_update`, `is_allow_delete`, `function_id_id`, `module_id_id`, `is_show`, `user_group_id_id`) VALUES
	(13, 1, 1, 1, 1, 8, 1, 1, 2),
	(14, 1, 1, 1, 1, 10, 1, 1, 2),
	(15, 1, 1, 1, 1, 11, 1, 1, 2),
	(16, 1, 1, 1, 1, 9, 1, 1, 2),
	(17, 1, 1, 1, 1, 12, 2, 1, 2),
	(18, 1, 1, 1, 1, 13, 2, 1, 2),
	(19, 1, 1, 1, 1, 12, 2, 1, 3);
/*!40000 ALTER TABLE `privileges_privileges` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.users_users
DROP TABLE IF EXISTS `users_users`;
CREATE TABLE IF NOT EXISTS `users_users` (
  `user_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `updated_date` datetime(6) NOT NULL,
  `user_group_id_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`user_name`),
  UNIQUE KEY `users_users_user_name_10b95449_uniq` (`user_name`),
  KEY `user_group_id_id` (`user_group_id_id`),
  CONSTRAINT `users_use_user_group_id_id_e0e2f342_fk_user_groups_usergroups_id` FOREIGN KEY (`user_group_id_id`) REFERENCES `user_groups_usergroups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.users_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users_users` DISABLE KEYS */;
INSERT INTO `users_users` (`user_name`, `password`, `email`, `is_active`, `created_date`, `updated_date`, `user_group_id_id`, `name`) VALUES
	('admin', 'admin', 'admin@gmail.com', 1, '0000-00-00 00:00:00.000000', '2017-02-25 23:35:16.235812', 2, 'Admin'),
	('employee', 'employee', 'employee@employee.com', 1, '2017-02-25 23:47:01.704991', '2017-02-26 00:37:40.469387', 3, 'Employee');
/*!40000 ALTER TABLE `users_users` ENABLE KEYS */;


-- Dumping structure for table base_framework_django.user_groups_usergroups
DROP TABLE IF EXISTS `user_groups_usergroups`;
CREATE TABLE IF NOT EXISTS `user_groups_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table base_framework_django.user_groups_usergroups: ~1 rows (approximately)
/*!40000 ALTER TABLE `user_groups_usergroups` DISABLE KEYS */;
INSERT INTO `user_groups_usergroups` (`id`, `name`) VALUES
	(2, 'Administrator'),
	(3, 'Employee');
/*!40000 ALTER TABLE `user_groups_usergroups` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
