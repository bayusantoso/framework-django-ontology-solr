import sys
import pandas as pd

def create_file(name):
    try:
        file = open(name,'w')
        file.close()
    except Exception as e:
        print('Something went wrong! Error : ', repr(e))
        sys.exit(0)
    

def run_eval(csv_file):
    dataframe = pd.read_csv(csv_file)
    
    for query in dataframe:
        try:
            create_file(query + '.csv')
        except:
            continue

run_eval('kueri.csv')
