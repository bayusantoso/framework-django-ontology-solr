import pandas as pd
import requests
import operator
from operator import itemgetter

mRecallResult = {}
def run_eval(csv_file):
    dataframe = pd.read_csv(csv_file)
    
    for query in dataframe:
        try:
            query = query.lstrip()
            query_result = get_result(query)
            onto_retrieved = get_onto_id(query_result)

            onto_goldenlist = []
            golden_list = pd.read_csv(query + ".csv")
            for item in golden_list:
                onto_goldenlist.append(item.strip())
            mRecallResult[query] = interpolated_max(onto_retrieved, onto_goldenlist)
            #if query == 'Wood':
            #    print(get_precision(onto_retrieved, onto_goldenlist))
            #    print(get_recall(onto_retrieved, onto_goldenlist))
                      
            print(query)
            print(onto_retrieved)
            print(onto_goldenlist)
            '''
            if query == 'Cavity':
                print('Cavity')
                print(onto_retrieved)
                print(onto_goldenlist)
                print(mRecallResult[query])
            '''
        except Exception as e:
            print(e)
            continue

    return mRecallResult


def get_result(query):
    solr_url = "http://localhost:8983/solr/geneontology/select"
    url = solr_url + "?q=" + query + "&start=0&rows=100000&wt=json"
    query_result = requests.get(url)
    return query_result

def get_onto_id(response):
    go_onto = []
    result = response.json()

    if result == None:
        return go_onto

    result_response = None
    if "response" in result:
        result_response = result["response"]

    if result_response == None:
        return go_onto

    if len(result_response["docs"]) > 0:
        num = 1
        for doc in result_response["docs"]:
            go_id = doc["id"]
            go_onto.append(go_id)

    return go_onto

def average_recall():
        avg_recall = {}
        sum_recall = {}
        titik_recall = 0
        test = {}
        while titik_recall <= 1:
            try:
                a = []
                for item in mRecallResult:
                    for itemResult in mRecallResult[item]:
                        if itemResult == titik_recall:
                            print(item, ' ', titik_recall, ' ', (mRecallResult[item][itemResult] * 100))
                            a.append((mRecallResult[item][itemResult] * 100))
                            if itemResult in sum_recall.keys():
                                sum_recall[itemResult] = sum_recall[itemResult] + (mRecallResult[item][itemResult] * 100)
                            else:
                                sum_recall[itemResult] = mRecallResult[item][itemResult] * 100
                test[titik_recall] = a
                titik_recall += 0.1
                titik_recall = round(titik_recall,1)
                        
            except Exception as e:
                print(e)
                break

        r_standar = 0
        
        while r_standar <= 1.0:
            #print(test[r_standar])
            #print(r_standar, ' : ' , sum_recall[r_standar], ' -> ', sum_recall[r_standar]/20)
            avg_recall[str(r_standar)] = (sum_recall[r_standar]/20)
            r_standar += 0.1
            r_standar = round(r_standar,1)
         
        return avg_recall

def get_relevant_item_count(retrieved, golden_list):
    relevant_item_retrieved = 0
    for item in retrieved:
        if item in golden_list:
            relevant_item_retrieved = relevant_item_retrieved + 1

    return relevant_item_retrieved

def get_precision(retrieved, golden_list):
    item_retrieved = len(retrieved)

    relevant_item_retrieved = get_relevant_item_count(retrieved, golden_list)

    precision_point = 0
    if item_retrieved > 0:
        precision_point = (relevant_item_retrieved / item_retrieved)
    
    return precision_point

def get_recall(retrieved, golden_list):
    item_relevant = len(golden_list)

    relevant_item_retrieved = get_relevant_item_count(retrieved, golden_list)

    recall_point = 0
    if item_relevant > 0:
        recall_point = (relevant_item_retrieved / item_relevant)
    
    return recall_point

def get_interpolated_precision_recall_curve(retrieved, golden_list):
    recall_point = {}

    return recall_point

def evaluation(retrieved, golden_list):
    precision_point = get_precision(retrieved, golden_list)
    recall_point = get_recall(retrieved, golden_list)
    print('precision :', precision_point)
    print('recall :', recall_point)
    
    return recall_point

def interpolated_max(retrieved_list, golden_list):
    retrieved = 0
    relevant = 0
    golden_list_count = len(golden_list)
    recall = []
    precision = []
    retrieved = 0
    
    for i in retrieved_list:
        retrieved += 1
        if i in golden_list:
            relevant += 1
            recall_point = (relevant / golden_list_count)
            recall.append(round(recall_point,4))
            precision_point = (relevant / retrieved)
            precision.append(round(precision_point,4))

    zips = zip(precision,recall)
    a = list(zips)
    titik_recall = 0
    recall_point = {}
    
    while titik_recall <= 1:
        try:
            d = 0
            c = []
            titik_recall = round(titik_recall,1)
            while d < len(a):
                if a[d][1] >= titik_recall:
                    c.extend(a[d])
                d += 1
            
            x = 0
            fix = []
            while x < len(c):
                if x%2 == 0:
                    fix.append(c[x])
                x += 1
            
            maks = 0
            if len(fix):
                maks = max(fix)
            
            recall_point[titik_recall] = maks
            titik_recall += 0.1
        except Exception as e:
            print(e)
            break
    return recall_point

cek = run_eval("kueri.csv")
average_recall()

