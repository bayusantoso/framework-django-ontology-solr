from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'searchgo_solr/$', views.searchgo_solr, name='searchgo_solr'),
    url(r'resultgo_solr/$', views.resultgo_solr, name='resultgo_solr'),
]