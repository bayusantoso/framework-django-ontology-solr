# baseframework/models.py
from django.db import models

# Create your models here.
class UserType(models.Model):
    Name = models.CharField(max_length=50)

# class FunctionInfo(models.Model):
#    Id = models.CharField(max_length=30)
#    Name = models.CharField(max_length=50)
#    UserType = UserType()

class GOntology(models.Model):
    go_index = models.CharField(max_length=100)
    go_id = models.CharField(max_length=100)
    go_name = models.CharField(max_length=200)
    go_definition = models.CharField(max_length=2000)
    go_synonym = []
    go_is_a = []
    go_part_of = []

    def __unicode__(self):
        return self.title