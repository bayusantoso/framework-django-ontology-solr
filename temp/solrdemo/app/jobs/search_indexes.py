import datetime
from haystack import indexes
from .models import GOntology

class GOntologyIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    id = indexes.CharField(model_attr='go_id')
    go_name = indexes.CharField(model_attr='go_name')
    go_definition = indexes.CharField(model_attr='go_definition')

    def get_model(self):
        return GOntology

    def index_queryset(self, using=None):
        return self.get_model().objects.all()
