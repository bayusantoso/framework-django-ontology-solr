# **Django Ontology SOLR**

Django Ontology SOLR adalah suatu _framework_ yang mengintegrasikan Django dengan Search Engine SOLR.

**Instalasi**

Prerequisition:

- Python 3.4

- Django 1.9.0

- SOLR 5

- MySql

Library yang digunakan :

- MySqlDB or PyMySql

- MakoTemplate

- request

- pandas