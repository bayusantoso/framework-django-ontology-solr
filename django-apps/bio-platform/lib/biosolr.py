import os
import os.path
import requests

from django.shortcuts import render
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse
from django.middleware import csrf

from .gomodel import GOntology

class BioSolr:
    str_parent = []
    span_parent = {}
    solr_url = ""
    last_query = ""

    def __init__(self):
        str_parent = []
        span_parent = []

    def getListResult(self, query):
        query_result = None
        if query != None and query != "":
            url = self.solr_url + "?q=" + query + "&start=0&rows=100000&wt=json"
            query_result = requests.get(url)

        result = None

        if query_result != None:
            result = self.bindJsonToListOfObject(query_result)

        return result

    def getListResultExpandedQuery(self, query, deep):
        query_result = None
        if query != None and query != "":
            url = self.solr_url + "?q=" + query + "&start=0&rows=100000&wt=json"
            query_result = requests.get(url)

        result = None

        if query_result != None:
            result = self.bindJsonToListOfObject(query_result)

        query_expanded = ""
        if result != None:
            for item in result:
                if deep == 0:
                    break

                for is_a_item in item.go_is_a:
                    itemObj = self.getObjectResult(is_a_item[31:])
                    if itemObj != None:
                        query_expanded += " " + itemObj.go_name

                deep -= 1
        query = query + " " + query_expanded
        self.last_query = query
        return self.getListResult(query)

    def getObjectResult(self, id):
        query_result = None
        if id != None and id != "":
            url = self.solr_url + "?q=id:\"" + id + "\"&wt=json"
            query_result = requests.get(url)

        result = None
        if query_result != None:
            result = self.bindJsonToObject(query_result, id)

        return result

    def getOntoChilds(self, id):
        go_onto = None

        if id == "all":
            return None

        go_onto = None
        if id != None and id != "":
            url = self.solr_url + "?q=is_a:\"" + id + "\"&wt=json"
            query_result = requests.get(url)

            if query_result != None:
                go_onto = self.bindJsonToListOfObject(query_result)

        return go_onto

    def getObjectHierarchy(obj):
        if obj == None:
            return ""

        if len(obj.go_hierarcy) <= 0:
            return ""

        str = ""
        item = obj.go_hierarcy[0]
        if item != None:
            if item.go_id == "all":
                str += item.go_id
            else:
                str += item.go_id + " | "

        return str

    def getSolrHierarchy(self, id):
        go_onto = None

        if id == "all":
            return None

        if id != None and id != "":
            url = self.solr_url + "?q=id:\"" + id + "\"&wt=json"
            query_result = requests.get(url)

            if query_result != None:
                go_onto = self.bindJsonToObject(query_result, id)

        return go_onto

    def bindJsonToObject(self, response, id):
        go_onto = None
        result = response.json()
        if result == None:
            return go_onto

        result_response = None
        if "response" in result:
            result_response = result["response"]

        if result_response == None:
            return go_onto

        if len(result_response["docs"]) > 0:
            doc = result_response["docs"][0]
            gene_ontology = GOntology()
            if "id" in doc:
                gene_ontology.go_id = doc["id"]
                gene_ontology.go_id_alt = doc["id"].replace(":","_")
            if "go_name" in doc:
                gene_ontology.go_name = doc["go_name"]
            if "go_definition" in doc:
                gene_ontology.go_definition = doc["go_definition"]

            if "is_a" in doc:
                gene_ontology.go_is_a = []
                num = 1
                for is_a in doc["is_a"]:
                    if num > 1:
                        break

                    gene_ontology.go_is_a.append(is_a[31:])
                    object_of = self.getSolrHierarchy(is_a[31:])
                    if object_of != None:
                        self.str_parent.append(object_of)
                        if len(self.str_parent) > 0:
                            str_span = 0

                            for item in self.str_parent:
                                str_span += 10

                            self.span_parent[object_of.go_id] = str_span
                        num += 1


            if "go_synonym" in doc:
                gene_ontology.go_synonym = doc["go_synonym"]

            go_onto = gene_ontology

        return go_onto

    def bindJsonToListOfObject(self, response):
        go_onto = []
        result = response.json()

        if result == None:
            return go_onto

        result_response = None
        if "response" in result:
            result_response = result["response"]

        if result_response == None:
            return go_onto

        if len(result_response["docs"]) > 0:
            num = 1
            for doc in result_response["docs"]:
                gene_ontology = GOntology()

                go_id = doc["id"]
                gene_ontology.go_num = num
                gene_ontology.go_id = go_id
                gene_ontology.go_name = doc["go_name"]
                gene_ontology.go_definition = doc["go_definition"]

                if "is_a" in doc:
                    gene_ontology.go_is_a = doc["is_a"]

                if "go_synonym" in doc:
                    gene_ontology.go_synonym = doc["go_synonym"]

                num += 1
                go_onto.append(gene_ontology)

        return go_onto
