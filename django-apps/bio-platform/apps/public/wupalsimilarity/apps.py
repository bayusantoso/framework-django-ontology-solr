from django.apps import AppConfig


class WupalsimilarityConfig(AppConfig):
    name = 'wupalsimilarity'
