from .models import Users
from .daos import UsersDao as moduleDao

class UserService():

    def SaveUser(obj):
        result = moduleDao.SaveUser(obj)
        return result

    def GetUser(id):
        result = moduleDao.GetUser(id)
        return result

    def GetUsers(self):
        result = moduleDao.GetUsers(self)
        return result

    def DeleteUser(id):
        result = moduleDao.DeleteUser(id)
        return result
