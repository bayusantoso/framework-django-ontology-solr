from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError

from .models import Users

class UsersDao():
    def SaveUser(obj):
        try:
            obj.save()
            return True
        except IntegrityError as e:
            return e.message

    def DeleteUser(id):
        try:
            obj = Users.objects.get(pk=id)
            return obj.delete()
        except IntegrityError as e:
            return e.message

    def GetUser(id):
        try:
            obj = Users.objects.get(pk=id)
            return obj
        except IntegrityError as e:
            return e.message

    def GetUsers(self):
        try:
            obj = Users.objects.all()
            return obj
        except IntegrityError as e:
            return e.message
