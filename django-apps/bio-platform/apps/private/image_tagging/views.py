# apps/private/image_taging/views.py
import os
import os.path
import requests
import html

from django.shortcuts import render, redirect
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse, HttpResponseRedirect
from django.middleware import csrf
from django.conf import settings

from .models import ImageTaging
from .services import ImageTagingService as image_taging_service
from lib.basecontroller import BaseController as base_controller

# variable
base_url = settings.BASE_URL
master_view = settings.PRIVATE_MASTER_VIEW
template_path = 'private/image_taging/'
function_id = 'image_taging'

# Create your views here.
def index(request):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowRead(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)
    objs = image_taging_service.GetImageTagings(image_taging_service)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token, 'model' : objs})
    return render(request, master_view, {'content':content,'base_url':base_url})

def create(request):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowCreate(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = ImageTaging()
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            image_taging_service.SaveImageTaging(obj)
            return HttpResponseRedirect('/manage/imagetaging/detail/' + str(obj.pk))

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj})
    return render(request, master_view, {'content':content,'base_url':base_url})

def edit(request, pk):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowUpdate(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = ImageTaging()
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            image_taging_service.SaveImageTaging(obj)
            return HttpResponseRedirect('/manage/imagetaging/detail/' + str(obj.pk))
    else:
        obj = image_taging_service.GetImageTaging(pk)

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url})
    return render(request, master_view, {'content':content,'base_url':base_url})

def detail(request, pk):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowRead(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = image_taging_service.GetImageTaging(pk)
    taging_data = None
    if obj != None:
        taging_data = html.unescape(obj.taging_data)

    content = render_to_string(template_path + "detail.html", {'csrf_token':csrf_token, 'model' : obj, 'taging_data' : taging_data, 'base_url':base_url})
    return render(request, master_view, {'content':content, 'show_taggd' : True ,'base_url':base_url})

def delete(request, pk):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowDelete(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)
    image_taging_service.DeleteImageTaging(pk)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token})
    return redirect(base_url + "manage/imagetaging")

def bind_data_to_object(request):
    obj = ImageTaging()
    id = request.POST.get("id", "")
    if id != None and id != 'None':
        obj.pk = id
    preview = request.POST.get("preview", "")
    obj.name = request.POST.get("name", "")

    try:
        obj.image_file = request.FILES['image_file']
    except:
        obj.image_file = preview

    obj.taging_data = request.POST.get("output_minify", "")

    return obj

def validate_data(request):
    return True
