from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError

from .models import ImageTaging

class ImageTagingDao():
    def SaveImageTaging(obj):
        try:
            obj.save()
            return True
        except IntegrityError as e:
            return e.message

    def DeleteImageTaging(id):
        try:
            obj = ImageTaging.objects.get(pk=id)
            return obj.delete()
        except IntegrityError as e:
            return e.message

    def GetImageTaging(id):
        try:
            obj = ImageTaging.objects.get(pk=id)
            return obj
        except IntegrityError as e:
            return e.message

    def GetImageTagings(self):
        try:
            obj = ImageTaging.objects.all()
            return obj
        except IntegrityError as e:
            return e.message
