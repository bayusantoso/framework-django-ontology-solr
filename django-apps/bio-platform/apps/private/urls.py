from django.conf.urls import url, patterns

from .home import views as home
from .functions import views as functions
from .modules import views as modules
from .privileges import views as privileges
from .user_groups import views as user_groups
from .users import views as users
from .image_tagging import views as image_taging
from .golden_list import views as golden_list
from .evaluation import views as evaluation
from .login import views as login

url_home = [
    url(r'^$', home.index , name='private_home_index'),
    url(r'leftmenu/$', home.get_menu , name='private_get_menu'),
    url(r'notallowed/$', home.notallowed, name='private_notallowed'),
    url(r'getloginname/$', home.getloginname, name='private_getloginname'),
]

url_functions = [
    url(r'functions/$', functions.index , name='private_functions_index'),
    url(r'functions/create$', functions.create, name='private_functions_create'),
    url(r'functions/edit/(?P<pk>\d+)$', functions.edit, name='private_functions_edit'),
    url(r'functions/detail/(?P<pk>\d+)$', functions.detail, name='private_functions_detail'),
    url(r'functions/delete/(?P<pk>\d+)$', functions.delete, name='private_functions_delete'),
]

url_modules = [
    url(r'modules/$', modules.index , name='private_modules_index'),
    url(r'modules/create$', modules.create , name='private_modules_create'),
    url(r'modules/edit/(?P<pk>\d+)$', modules.edit , name='private_modules_edit'),
    url(r'modules/detail/(?P<pk>\d+)$', modules.detail , name='private_modules_detail'),
    url(r'modules/delete/(?P<pk>\d+)$', modules.delete , name='private_modules_delete'),
]

url_users = [
    url(r'users/$', users.index , name='private_users_index'),
    url(r'users/create$', users.create , name='private_users_create'),
    url(r'users/edit/(?P<pk>.*)$', users.edit , name='private_users_edit'),
    url(r'users/detail/(?P<pk>.*)$', users.detail , name='private_users_detail'),
    url(r'users/delete/(?P<pk>.*)$', users.delete , name='private_users_delete'),
]

url_user_groups = [
    url(r'usergroups/$', user_groups.index , name='private_user_groups_index'),
    url(r'usergroups/create$', user_groups.create , name='private_user_groups_create'),
    url(r'usergroups/edit/(?P<pk>\d+)$', user_groups.edit , name='private_user_groups_edit'),
    url(r'usergroups/detail/(?P<pk>\d+)$', user_groups.detail , name='private_user_groups_detail'),
    url(r'usergroups/delete/(?P<pk>\d+)$', user_groups.delete , name='private_user_groups_delete'),
]

url_privileges = [
    url(r'privileges/$', privileges.index , name='private_privileges_index'),
    url(r'privileges/create$', privileges.create , name='private_privileges_create'),
    url(r'privileges/edit/(?P<pk>\d+)$', privileges.edit , name='private_privileges_edit'),
    url(r'privileges/detail/(?P<pk>\d+)$', privileges.detail , name='private_privileges_detail'),
    url(r'privileges/delete/(?P<pk>\d+)$', privileges.delete , name='private_privileges_delete'),
]

url_image_taging = [
    url(r'imagetaging/$', image_taging.index , name='private_image_taging_index'),
    url(r'imagetaging/create$', image_taging.create, name='private_image_taging_create'),
    url(r'imagetaging/edit/(?P<pk>\d+)$', image_taging.edit, name='private_image_taging_edit'),
    url(r'imagetaging/detail/(?P<pk>\d+)$', image_taging.detail, name='private_image_taging_detail'),
    url(r'imagetaging/delete/(?P<pk>\d+)$', image_taging.delete, name='private_image_taging_delete'),
]

url_golden_list = [
    url(r'goldenlist/$', golden_list.index , name='private_golden_list_index'),
    url(r'goldenlist/create$', golden_list.create, name='private_golden_list_create'),
    url(r'goldenlist/edit/(?P<pk>\d+)$', golden_list.edit, name='private_golden_list_edit'),
    url(r'goldenlist/detail/(?P<pk>\d+)$', golden_list.detail, name='private_golden_list_detail'),
    url(r'goldenlist/delete/(?P<pk>\d+)$', golden_list.delete, name='private_golden_list_delete'),
]

url_evaluation = [
    url(r'evaluation/$', evaluation.index, name='private_evaluation_index'),
]

url_login = [
    url(r'login/$', login.index, name='private_login_index'),
    url(r'login/logout$', login.logout, name='private_logout_index'),
]

urlpatterns = url_home + \
              url_functions + \
              url_modules + \
              url_users + \
              url_user_groups + \
              url_privileges + \
              url_image_taging + \
              url_golden_list + \
              url_evaluation + \
              url_login
