from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError

from apps.private.users.models import Users
from apps.private.privileges.models import Privileges

class LoginDao():

    def GetUser(id):
        try:
            obj = Users.objects.get(pk=id)
            return obj
        except IntegrityError as e:
            return e.message

    def GetPrivilege(functionid, user_groupid):
        try:
            obj = Privileges.objects.get(function_id=functionid, user_group_id=user_groupid)
            return obj
        except IntegrityError as e:
            return e.message


