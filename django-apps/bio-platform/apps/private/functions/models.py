from django.db import models

# Create your models here.
class Functions(models.Model):
    function_id = models.CharField(max_length=100, default='', editable=True)
    name = models.CharField(max_length=100, default='', editable=True)
    url = models.CharField(max_length=100, default='', editable=True)
    sequence = models.IntegerField(default=0, editable=True)
    is_active = models.BooleanField(max_length=1, default=False, editable=True)
    is_show = models.BooleanField(max_length=1, default=False, editable=True)
    is_public = models.BooleanField(max_length=1, default=False, editable=True)
    icon = models.FileField(upload_to='static/images/%Y/%m/%d', default='')

    def __unicode__(self):
        return self.name
