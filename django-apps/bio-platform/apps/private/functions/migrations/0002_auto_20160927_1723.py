# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-09-27 10:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('functions', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='functions',
            name='function_id',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='functions',
            name='name',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='functions',
            name='url',
            field=models.CharField(default='', max_length=100),
        ),
    ]
