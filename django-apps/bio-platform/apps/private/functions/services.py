from .models import Functions
from .daos import FunctionsDao as functionDao
class FunctionService():

    def SaveFunction(obj):
        result = functionDao.SaveFunction(obj)
        return result

    def GetFunction(id):
        result = functionDao.GetFunction(id)
        return result

    def GetFunctions(self):
        result = functionDao.GetFunctions(self)
        return result

    def DeleteFunction(id):
        result = functionDao.DeleteFunction(id)
        return result
