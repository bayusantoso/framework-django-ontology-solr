# apps/private/functions/views.py
import os
import os.path
import requests

from django.shortcuts import render, redirect
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse, HttpResponseRedirect
from django.middleware import csrf
from django.conf import settings

from .models import Functions
from .services import FunctionService as function_service
from lib.basecontroller import BaseController

# variable
base_url = settings.BASE_URL
master_view = settings.PRIVATE_MASTER_VIEW
template_path = 'private/functions/'
function_id = 'functions'

# Create your views here.
def index(request):
    base_controller = BaseController()
    if base_controller.isLogin(request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowRead(request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)
    objs = function_service.GetFunctions(function_service)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token, 'model' : objs})
    return render(request, master_view, {'content':content,'base_url':base_url})

def create(request):
    base_controller = BaseController()
    if base_controller.isLogin(request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowCreate(request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = Functions()
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            function_service.SaveFunction(obj)
            return HttpResponseRedirect('/manage/functions/detail/' + str(obj.pk))

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url})
    return render(request, master_view, {'content':content,'base_url':base_url})

def edit(request, pk):
    base_controller = BaseController()
    if base_controller.isLogin(request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowUpdate(request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = Functions()
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            function_service.SaveFunction(obj)
            return HttpResponseRedirect('/manage/functions/detail/' + str(obj.pk))
    else:
        obj = function_service.GetFunction(pk)

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url})
    return render(request, master_view, {'content':content,'base_url':base_url})

def detail(request, pk):
    base_controller = BaseController()
    if base_controller.isLogin(request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowRead(request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = function_service.GetFunction(pk)

    content = render_to_string(template_path + "detail.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url})
    return render(request, master_view, {'content':content,'base_url':base_url})

def delete(request, pk):
    base_controller = BaseController()
    if base_controller.isLogin(request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowDelete(request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)
    function_service.DeleteFunction(pk)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token})
    return redirect(base_url + "manage/functions")

def bind_data_to_object(request):
    obj = Functions()
    id = request.POST.get("id", "")
    if id != None and id != 'None':
        obj.pk = id

    obj.function_id = request.POST.get("function_id", "")
    obj.name = request.POST.get("name", "")
    obj.url = request.POST.get("url", "")

    sequence = request.POST.get("sequence", "")
    try:
        if sequence != None:
            obj.sequence = int(sequence)
        else:
            obj.sequence = 0
    except ValueError:
        obj.sequence = 0

    is_active = request.POST.get("is_active", "")
    if is_active != None:
        obj.is_active = bool(is_active)

    is_show = request.POST.get("is_show", "")
    if is_show != None:
        obj.is_show = bool(is_show)

    is_public = request.POST.get("is_public", "")
    if is_public != None:
        obj.is_public = bool(is_public)
        
    preview = request.POST.get("preview", "")
    try:
        obj.icon = request.FILES['image_file']
    except:
        obj.icon = preview

    return obj

def validate_data(request):
    return True
