# apps/private/modules/views.py
import os
import os.path
import requests

from django.shortcuts import render, redirect
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse, HttpResponseRedirect
from django.middleware import csrf
from django.conf import settings

from .models import Modules
from .services import ModuleService as module_service
from lib.basecontroller import BaseController as base_controller

# variable
base_url = settings.BASE_URL
master_view = settings.PRIVATE_MASTER_VIEW
template_path = 'private/modules/'
function_id = 'modules'

# Create your views here.
def index(request):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowRead(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)
    objs = module_service.GetModules(module_service)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token, 'model' : objs})
    return render(request, master_view, {'content':content,'base_url':base_url})

def create(request):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowCreate(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = Modules()
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            module_service.SaveModule(obj)
            return HttpResponseRedirect('/manage/modules/detail/' + str(obj.pk))

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url})
    return render(request, master_view, {'content':content,'base_url':base_url})

def edit(request, pk):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowUpdate(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = Modules()
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            module_service.SaveModule(obj)
            return HttpResponseRedirect('/manage/modules/detail/' + str(obj.pk))
    else:
        obj = module_service.GetModule(pk)

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url})
    return render(request, master_view, {'content':content,'base_url':base_url})

def detail(request, pk):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowRead(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = module_service.GetModule(pk)

    content = render_to_string(template_path + "detail.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url})
    return render(request, master_view, {'content':content,'base_url':base_url})

def delete(request, pk):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowDelete(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)
    module_service.DeleteModule(pk)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token})
    return redirect(base_url + "manage/modules")

def bind_data_to_object(request):
    obj = Modules()
    id = request.POST.get("id", "")
    if id != None and id != 'None':
        obj.pk = id

    obj.name = request.POST.get("name", "")

    return obj

def validate_data(request):
    return True
