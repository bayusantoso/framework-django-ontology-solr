from django.db import models
from apps.private.user_groups.models import UserGroups
from apps.private.functions.models import Functions
from apps.private.modules.models import Modules
from .models import Privileges
import django_filters
# Create your models here.
class PrivilegeFilter(django_filters.FilterSet):
    class Meta:
        model = Privileges
        fields = ['user_group_id']
