from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError

from .models import Privileges

class PrivilegesDao():
    def SavePrivilege(obj):
        try:
            obj.save()
            return True
        except IntegrityError as e:
            return e.message

    def DeletePrivilege(id):
        try:
            obj = Privileges.objects.get(pk=id)
            return obj.delete()
        except IntegrityError as e:
            return e.message

    def GetPrivilege(id):
        try:
            obj = Privileges.objects.get(pk=id)
            return obj
        except IntegrityError as e:
            return e.message

    def GetPrivileges(self, filter):
        try:
            obj = None
            if filter != None:
                obj = Privileges.objects.filter(filter)
            else:
                obj = Privileges.objects.all()

            return obj
        except IntegrityError as e:
            return e.message
