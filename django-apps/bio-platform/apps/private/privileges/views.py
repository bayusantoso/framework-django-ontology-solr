# apps/private/user_groups/views.py
import os
import os.path
import requests

from django.shortcuts import render, redirect
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse, HttpResponseRedirect
from django.middleware import csrf
from django.conf import settings

from apps.private.user_groups.models import UserGroups
from apps.private.user_groups.services import UserGroupService as user_group_service

# variable
base_url = settings.BASE_URL
master_view = settings.PRIVATE_MASTER_VIEW
template_path = 'private/user_groups/'

# Create your views here.
def index(request):
    csrf_token = csrf.get_token(request)
    objs = user_group_service.GetUserGroups(user_group_service)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token, 'model' : objs})
    return render(request, master_view, {'content':content,'base_url':base_url})

def create(request):
    csrf_token = csrf.get_token(request)

    obj = UserGroups()
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            user_group_service.SaveUserGroup(obj)
            return HttpResponseRedirect('/manage/user_groups/detail/' + str(obj.pk))

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url})
    return render(request, master_view, {'content':content,'base_url':base_url})

def edit(request, pk):
    csrf_token = csrf.get_token(request)

    obj = UserGroups()
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            user_group_service.SaveUserGroup(obj)
            return HttpResponseRedirect('/manage/user_groups/detail/' + str(obj.pk))
    else:
        obj = user_group_service.GetUserGroup(pk)

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url})
    return render(request, master_view, {'content':content,'base_url':base_url})

def detail(request, pk):
    csrf_token = csrf.get_token(request)

    obj = user_group_service.GetUserGroup(pk)

    content = render_to_string(template_path + "detail.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url})
    return render(request, master_view, {'content':content,'base_url':base_url})

def delete(request, pk):
    csrf_token = csrf.get_token(request)
    user_group_service.DeleteUserGroup(pk)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token})
    return redirect(base_url + "manage/user_groups")

def bind_data_to_object(request):
    obj = UserGroups()
    id = request.POST.get("id", "")
    if id != None and id != 'None':
        obj.pk = id

    obj.name = request.POST.get("name", "")

    return obj

def validate_data(request):
    return True
