# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2017-02-25 11:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('modules', '0001_initial'),
        ('user_groups', '0001_initial'),
        ('functions', '0008_functions_icon'),
    ]

    operations = [
        migrations.CreateModel(
            name='Privileges',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_allow_read', models.BooleanField(default=False, max_length=1)),
                ('is_allow_create', models.BooleanField(default=False, max_length=1)),
                ('is_allow_update', models.BooleanField(default=False, max_length=1)),
                ('is_allow_delete', models.BooleanField(default=False, max_length=1)),
                ('function_id', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='functions.Functions')),
                ('module_id', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='modules.Modules')),
                ('user_group_id', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='user_groups.UserGroups')),
            ],
        ),
    ]
