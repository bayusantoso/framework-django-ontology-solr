from django.db import models
from apps.private.user_groups.models import UserGroups
from apps.private.functions.models import Functions
from apps.private.modules.models import Modules
# Create your models here.
class Privileges(models.Model):
    function_id = models.OneToOneField(Functions, unique=False, blank=True, null=True)
    user_group_id = models.ForeignKey(UserGroups, models.SET_NULL, blank=True, null=True)
    module_id = models.OneToOneField(Modules, unique=False, blank=True, null=True)
    is_allow_read = models.BooleanField(max_length=1, default=False, editable=True)
    is_allow_create = models.BooleanField(max_length=1, default=False, editable=True)
    is_allow_update = models.BooleanField(max_length=1, default=False, editable=True)
    is_allow_delete = models.BooleanField(max_length=1, default=False, editable=True)
    is_show = models.BooleanField(max_length=1, default=False, editable=True)

    def __unicode__(self):
        return self.function_id
