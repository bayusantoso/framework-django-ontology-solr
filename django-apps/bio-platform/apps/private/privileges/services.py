from .models import Privileges
from .daos import PrivilegesDao as moduleDao

class PrivilegeService():

    def SavePrivilege(obj):
        result = moduleDao.SavePrivilege(obj)
        return result

    def GetPrivilege(id):
        result = moduleDao.GetPrivilege(id)
        return result

    def GetPrivileges(self, filter):
        result = moduleDao.GetPrivileges(self, filter)
        return result

    def DeletePrivilege(id):
        result = moduleDao.DeletePrivilege(id)
        return result
