from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError

from .models import GoldenList

class GoldenListDao():
    def SaveGoldenList(obj):
        try:
            obj.save()
            return True
        except IntegrityError as e:
            return e.message

    def DeleteGoldenList(id):
        try:
            obj = GoldenList.objects.get(pk=id)
            return obj.delete()
        except IntegrityError as e:
            return e.message

    def GetGoldenList(id):
        try:
            obj = GoldenList.objects.get(pk=id)
            return obj
        except IntegrityError as e:
            return e.message

    def GetGoldenLists(self):
        try:
            obj = GoldenList.objects.all()
            return obj
        except IntegrityError as e:
            return e.message
