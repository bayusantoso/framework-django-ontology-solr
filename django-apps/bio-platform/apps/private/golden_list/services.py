from .models import GoldenList
from .daos import GoldenListDao as goldenListDao
class GoldenListService():

    def SaveGoldenList(obj):
        result = goldenListDao.SaveGoldenList(obj)
        return result

    def GetGoldenList(id):
        result = goldenListDao.GetGoldenList(id)
        return result

    def GetGoldenLists(self):
        result = goldenListDao.GetGoldenLists(self)
        return result

    def DeleteGoldenList(id):
        result = goldenListDao.DeleteGoldenList(id)
        return result
