from django.apps import AppConfig


class GoldenListConfig(AppConfig):
    name = 'golden_list'
