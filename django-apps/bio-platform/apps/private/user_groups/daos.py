from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError

from .models import UserGroups
from apps.private.privileges.models import Privileges
from apps.private.privileges.daos import PrivilegesDao as privilege_dao
from apps.private.privileges.filter import PrivilegeFilter

class UserGroupsDao():
    def SaveUserGroup(obj, childs):
        try:
            obj.save()
            if childs != None:
                for row in childs:
                    row.user_group_id = obj
                    privilege_dao.SavePrivilege(row)

            return True
        except IntegrityError as e:
            return e.message

    def UpdateUserGroup(obj, childs):
        try:
            obj.save()
            if childs != None:

                obj_detail = privilege_dao.GetPrivileges(privilege_dao, None)

                obj_filter = {}
                obj_filter["user_group_id"] = obj.pk
                obj_filtered = PrivilegeFilter(obj_filter,queryset=obj_detail)
                obj_filtered_ids = []

                for row in obj_filtered.qs:
                    obj_filtered_ids.append(row.pk)

                for row in childs:
                    row.user_group_id = obj
                    privilege_dao.SavePrivilege(row)
                    if int(row.pk) in obj_filtered_ids:
                        obj_filtered_ids.remove(int(row.pk))

                for id in obj_filtered_ids:
                    privilege_dao.DeletePrivilege(id)

            return True
        except IntegrityError as e:
            return e.message

    def DeleteUserGroup(id):
        try:
            obj = UserGroups.objects.get(pk=id)
            return obj.delete()
        except IntegrityError as e:
            return e.message

    def GetUserGroup(id):
        try:
            obj = UserGroups.objects.get(pk=id)
            return obj
        except IntegrityError as e:
            return e.message

    def GetUserGroups(self):
        try:
            obj = UserGroups.objects.all()
            return obj
        except IntegrityError as e:
            return e.message
