# apps/private/user_groups/views.py
import os
import os.path
import requests

from django.shortcuts import render, redirect
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse, HttpResponseRedirect
from django.middleware import csrf
from django.conf import settings

from .models import UserGroups
from apps.private.functions.models import Functions
from apps.private.functions.services import FunctionService as function_service
from apps.private.modules.models import Modules
from apps.private.modules.services import ModuleService as module_service
from apps.private.privileges.models import Privileges
from apps.private.privileges.services import PrivilegeService as privilege_service
from apps.private.privileges.filter import PrivilegeFilter
from .services import UserGroupService as user_group_service
from lib.basecontroller import BaseController as base_controller

# variable
base_url = settings.BASE_URL
master_view = settings.PRIVATE_MASTER_VIEW
template_path = 'private/user_groups/'
function_id = 'user_groups'

# Create your views here.
def index(request):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowRead(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)
    objs = user_group_service.GetUserGroups(user_group_service)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token, 'model' : objs})
    return render(request, master_view, {'content':content,'base_url':base_url})

def create(request):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowCreate(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = UserGroups()
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            obj_detail = bind_detail_to_object(request)
            user_group_service.SaveUserGroup(obj, obj_detail)
            return HttpResponseRedirect('/manage/usergroups/detail/' + str(obj.pk))

    function_combobox = get_functions(request)
    modules_combobox = get_modules(request)

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url, 'function_combobox' : function_combobox, 'modules_combobox' : modules_combobox})
    return render(request, master_view, {'content':content,'base_url':base_url})

def edit(request, pk):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowUpdate(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = UserGroups()
    obj_filtered = {}
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            obj_detail = bind_detail_to_object(request)
            user_group_service.UpdateUserGroup(obj, obj_detail)
            return HttpResponseRedirect('/manage/usergroups/detail/' + str(obj.pk))
    else:
        obj = user_group_service.GetUserGroup(pk)

        obj_detail =  privilege_service.GetPrivileges(privilege_service, None)

        obj_filter = {}
        obj_filter["user_group_id"] = pk
        obj_filtered = PrivilegeFilter(obj_filter,queryset=obj_detail)

    function_combobox = get_functions(request)
    modules_combobox = get_modules(request)

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj, 'obj_detail' : obj_filtered, 'base_url':base_url, 'function_combobox' : function_combobox, 'modules_combobox' : modules_combobox})
    return render(request, master_view, {'content':content,'base_url':base_url})

def detail(request, pk):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowRead(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = user_group_service.GetUserGroup(pk)
    obj_detail =  privilege_service.GetPrivileges(privilege_service, None)

    obj_filter = {}
    obj_filter["user_group_id"] = pk
    obj_filtered = PrivilegeFilter(obj_filter,queryset=obj_detail)

    content = render_to_string(template_path + "detail.html", {'csrf_token':csrf_token, 'model' : obj, 'obj_detail' : obj_filtered,'base_url':base_url})
    return render(request, master_view, {'content':content,'base_url':base_url})

def delete(request, pk):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowDelete(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)
    user_group_service.DeleteUserGroup(pk)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token})
    return redirect(base_url + "manage/usergroups")

def bind_data_to_object(request):
    obj = UserGroups()
    id = request.POST.get("id", "")
    if id != None and id != 'None':
        obj.pk = id

    obj.name = request.POST.get("name", "")

    return obj

def bind_detail_to_object(request):
    obj_privileges = []

    ids = request.POST.getlist("DetailsId")
    module_ids = request.POST.getlist("DetailsModuleId")
    function_ids = request.POST.getlist("DetailsFunctionId")
    is_allow_reads = request.POST.getlist("DetailsIsAllowRead")
    is_allow_creates = request.POST.getlist("DetailsIsAllowCreate")
    is_allow_updates = request.POST.getlist("DetailsIsAllowUpdate")
    is_allow_deletes = request.POST.getlist("DetailsIsAllowDelete")
    is_shows = request.POST.getlist("DetailsIsShow")

    index = 0
    for item in ids:
        obj_privilege = Privileges()
        if item != None and item != 'None' and item != '':
            obj_privilege.pk = item

        obj_function = Functions()
        obj_function.pk = function_ids[index]
        obj_module = Modules()
        obj_module.pk = module_ids[index]
        obj_privilege.function_id = obj_function
        obj_privilege.module_id = obj_module
        print(is_allow_reads[index])
        if is_allow_reads[index] != None:
            obj_privilege.is_allow_read = bool(int(is_allow_reads[index]))
        if is_allow_creates[index] != None:
            obj_privilege.is_allow_create = bool(int(is_allow_creates[index]))
        if is_allow_updates[index] != None:
            obj_privilege.is_allow_update = bool(int(is_allow_updates[index]))
        if is_allow_deletes[index] != None:
            obj_privilege.is_allow_delete = bool(int(is_allow_deletes[index]))
        obj_privilege.is_show = is_shows[index]

        obj_privileges.append(obj_privilege)
        index = index + 1

    return obj_privileges

def validate_data(request):
    return True

def get_modules(request):
    objs = module_service.GetModules(module_service)
    combobox_modules = ""

    combobox_modules += "<select name='DetailsModuleId' id='DetailsModuleId_COUNTER_' class='form-control input-sm'>"
    if objs != None:
        for row in objs:
            combobox_modules += "<option value='" + str(row.pk) + "'>" + row.name + "</option>"
    combobox_modules += "</select>"

    return combobox_modules

def get_functions(request):
    objs = function_service.GetFunctions(function_service)
    combobox_function = ""

    combobox_function += "<select name='DetailsFunctionId' id='DetailsFunctionId_COUNTER_' class='form-control input-sm'>"
    if objs != None:
        for row in objs:
            combobox_function += "<option value='" + str(row.pk) + "'>" + row.name + "</option>"
    combobox_function += "</select>"

    return combobox_function

