# apps/private/golden_list/views.py
import os
import os.path
import requests
import html

from django.shortcuts import render, redirect
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse, HttpResponseRedirect
from django.middleware import csrf
from django.conf import settings

from .models import Evaluation
from .services import Evaluation as evaluation_service
from lib.recallpercision import RecallPercision
from lib.basecontroller import BaseController as base_controller

# variable
base_url = settings.BASE_URL
master_view = settings.PRIVATE_MASTER_VIEW
template_path = 'private/evaluation/'
solr_url = settings.SOLR_SELECT_URL
function_id = 'evaluation'

# Create your views here.
def index(request):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowRead(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)
    objs = None
    recpres = RecallPercision()
    recpres.solrUrl = solr_url
    objs = recpres.run_eval("goldenlist/","kueri.csv")
    average_interpolated = recpres.average_recall()
    total_sum = recpres.mSumRecall
    obj_relevant = recpres.mQueryRelevant
    r_standar = 0
    str_label = "["
    str_chart = "["
    str_table = {}
    sum_table = ""
    avg_table = ""
    while r_standar <= 1:
        try:
            for key in average_interpolated:
                if key == r_standar:
                    str_chart += "[" + str(r_standar) + "," + str(round(average_interpolated[key],2)) + "]"
                    str_label += "[" + str(r_standar) + "]"
                    avg_table += "<td>" + str(round(average_interpolated[key],2)) + "</td>"

            for key in total_sum:
                if key == r_standar:
                    sum_table += "<td>" + str(round(total_sum[key],2)) + "</td>"

            for key, value in objs.items():
                str_item = ""
                if key in str_table.keys():
                    str_item = str_table[key]

                for key_item in value:
                    if key_item == r_standar:
                        str_item = str_item + "<td>" + str(round(value[key_item] * 100,2)) + "</td>"

                str_table[key] = str_item

            if r_standar != 1.0:
                str_chart += ", "
                str_label += ", "

            r_standar += 0.1
            r_standar = round(r_standar, 1)
        except Exception as e:
            str_chart += str(e) + ","
            r_standar += 0.1
            r_standar = round(r_standar, 1)

    str_label += "]"
    str_chart += "]"
    retrieved = recpres.mRetrieved
    golden_list = recpres.mGoldenList
    recall = recpres.mRecallQuery
    precision = recpres.mPrecisionQuery
    content = render_to_string(template_path + "index.html",
                                {
                                    'csrf_token':csrf_token,
                                    'model' : objs,
                                    'map' : average_interpolated,
                                    'retrieved' : retrieved,
                                    'golden_list' : golden_list,
                                    'chart_str' : str_chart,
                                    'str_label' : str_label,
                                    'str_table' : str_table,
                                    'sum_table' : sum_table,
                                    'avg_table' : avg_table,
                                    'obj_relevant' : GenerateRelevantTable(obj_relevant, recall, precision)
                                }
                               )
    return render(request, master_view, {'content':content,'base_url':base_url})

def GenerateRelevantTable(obj_relevant, recall, precision):
    str_table = ""
    value = ""
    for key in obj_relevant:
        value = str(obj_relevant[key])
        cnt_recall = str(round(recall[key], 2))
        cnt_precision = str(round(precision[key], 2))
        str_table += "<tr><td>" + key + "</td><td>" + value + "</td><td>" + cnt_recall + "</td><td>" + cnt_precision + "</td></tr>"

    return str_table


def GetEvaluations(obj):
    result = None
    return result

def GetGoldenList(self):
    return None

def GetRetrievedList(self):
    return None
