from django.db import models

# Create your models here.
class Evaluation(models.Model):
    name = models.CharField(max_length=100, default='', editable=True)
    created_by = models.CharField(max_length=100, default='', editable=True)
    created_date = models.DateTimeField(auto_now=False)
    updated_by = models.CharField(max_length=100, default='', editable=True)
    updated_date = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return self.name
